# API REST de Modelagem ER (TT 2022.1)

## 📓 Sobre o projeto

O projeto se trata de um trabalho realizado durante o **Treinamento Técnico EJCM 2022.1**, em que foi desenvolvida uma API REST com o tema do marketplace Civitas, feita pela Modelagem ER das entidades User (usuário), Purchase (compra) e Comment (comentário).


<hr><br>

## 🛠 Tecnologias e ferramentas

- [JavaScript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [Node.js](https://nodejs.org/pt-br/)
- [Express](https://expressjs.com/pt-br/)
- [Sequelize](https://sequelize.org/)


<hr><br>

## 💻 Como rodar o projeto?

<br>

#### ➡️ Clone esse repositório

```
git clone https://gitlab.com/vlimass/trabalho-backend-api-tt-ejcm.git
```

#### ➡️ Entre na pasta do projeto

```
cd trabalho-backend-api-tt-ejcm
```

#### ➡️ Renomeie o arquivo `.env.example` para `.env`

<br>

![Renomear arquivo .env.example para .env](./assets/imagem1_readme.png)

<br>

#### ➡️ Abra o terminal (pode ser pelo VSCode) e execute o comando para instalar as dependências

```
npm install
```

#### ➡️ Execute o comando para migrar o banco de dados

```
npm run migrate
```

#### ➡️ Execute o comando para seedar o banco de dados

```
npm run seed
```

#### ➡️ Execute o comando para servir a API

```
npm run start
```

- Se desejar servir em modo dev (requer o pacote [node-dev](https://www.npmjs.com/package/node-dev)), execute o comando:

```
npm run dev
``` 

#### ➡️ Tudo pronto! Sua API já está sendo servida. Para verificar as requests, basta inserir as rotas em uma plataforma de APIs como o Postman

<br>

![Verificar as requests](./assets/imagem2_readme.png)

<br>


#### ➡️ Para parar de servir a API, basta executar o comando `CTRL + C` no terminal

<br><hr>

<div align="center">
MIT License<br>
Copyright © 2022<br> 
Made with ❤️ by <b>Vinícius Lima</b> ✨
</div>
