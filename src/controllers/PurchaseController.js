const { response } = require('express') 
const User = require('../models/User')
const Purchase = require('../models/Purchase')

const show = async (req,res) => {
    const {id} = req.params

    try{
        const purchase = await Purchase.findByPk(id)
        return res.status(200).json({purchase})
    } catch(err) {
        return res.status(500).json({err})
    }
}

const index = async (req,res) => {
    try { 
        const purchases = await Purchase.findAll()
        return res.status(200).json({purchases})
    } catch(err) {
        return res.status(500).json({err})
    }
}

const create = async (req,res) => {
    try {
        const purchase = await Purchase.create(req.body)
        return res.status(201).json({
            message: "Compra criada com sucesso.", purchase: purchase
        })
    }catch(err) {
        res.status(500).json({error: err})
    }
}

const update = async (req,res) => {
    const {id} = req.params

    try {
        const [updated] = await Purchase.update(req.body, {where: {id: id}})
        if(updated){
            const purchase = await Purchase.findByPk(id)
            return res.status(200).send(purchase)
        }
        throw new Error()
    }catch(err) {
        return res.status(500).json("Compra não encontrada.")
    }
}

const destroy = async (req,res) => {
    const {id} = req.params

    try {
        const deleted = await Purchase.destroy({where: {id: id}})
        if(deleted){
            return res.status(200).json("Compra deletada com sucesso.")
        }
        throw new Error()
    }catch(err) {
        return res.status(500).json("Compra não encontrada.")
    }
}

const addRelationUser = async(req, res) => {
    const {id} = req.params
    
    try {
        const purchase = await Purchase.findByPk(id)
        const user = await User.findByPk(req.body.UserId)
        await purchase.setUser(user)

        return res.status(200).json(purchase)
    } catch(err) {
        return res.status(500).json({err})
    }
}

const removeRelationUser = async(req, res) => {
    const {id} = req.params

    try {
        const purchase = await Purchase.findByPk(id)
        await purchase.setUser(null)

        return res.status(200).json(purchase)
    } catch(err) {
        return res.status(500).json({err})
    }
}

module.exports = {
    show, 
    index,
    create, 
    update, 
    destroy,
    addRelationUser,
    removeRelationUser   
}