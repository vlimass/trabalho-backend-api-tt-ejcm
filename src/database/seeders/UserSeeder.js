const User = require("../../models/User");
const faker = require('faker-br');

const seedUser = async function (){
    const users = [];

    for (let i = 0; i<10; i++){
        users.push({
            name: faker.name.firstName(),
            email: faker.internet.email(),
            date_of_birth: faker.date.past(),
            address: faker.address.streetAddress(),
            gender: faker.name.gender(),
            phone_number: faker.phone.phoneNumber()
        });
    }

    try{
        await User.sync({force:true});
        await User.bulkCreate(users);
    }
    catch (err) {
        console.log(err);
    }
}

module.exports = seedUser;