require('../../config/dotenv')();
require('../../config/sequelize');

//const seedModel = require('./Model');
const seedUsers = require('./UserSeeder');
const seedPurchases = require('./PurchaseSeeder');
const seedComments = require('./CommentSeeder');


(async () => {
  try {
    await seedPurchases();
    await seedUsers();
    await seedComments();
    
  } catch(err) { console.log(err) }
})();
