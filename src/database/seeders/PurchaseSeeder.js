const Purchase = require("../../models/Purchase");
const faker = require('faker-br');

const seedPurchase = async function (){
    const purchases = []

    for(let i = 0; i < 10; i++){
        purchases.push({
            name: faker.commerce.productName(),
            price: faker.commerce.price(),
            supplier_name: faker.company.companyName(),
            isOnSale: faker.random.boolean()
        })
    }

    try{
        await Purchase.sync({force:true});
        await Purchase.bulkCreate(purchases);
    }
    catch (err) {
        console.log(err);
    }
}

module.exports = seedPurchase;