const { Router } = require('express');
const UserController = require('../controllers/UserController');
const PurchaseController = require('../controllers/PurchaseController');
const CommentController = require('../controllers/CommentController');


const router = Router()

router.get('/users/:id', UserController.show)
router.get('/users', UserController.index)
router.post('/users', UserController.create)
router.put('/users/:id', UserController.update)
router.delete('/users/:id', UserController.destroy)


router.get('/purchases/:id', PurchaseController.show)
router.get('/purchases', PurchaseController.index)
router.post('/purchases', PurchaseController.create)
router.put('/purchases/:id', PurchaseController.update)
router.delete('/purchases/:id', PurchaseController.destroy)
router.put('/purchaseadduser/:id', PurchaseController.addRelationUser)
router.delete('/purchaseremoveuser/:id', PurchaseController.removeRelationUser)


router.get('/comments/:id', CommentController.show)
router.get('/comments', CommentController.index)
router.post('/comments', CommentController.create)
router.put('/comments/:id', CommentController.update)
router.delete('/comments/:id', CommentController.destroy)
router.put('/commentadduserpurchase/:id', CommentController.addRelationUserPurchase)
router.delete('/commentremoveuserpurchase/:id', CommentController.removeRelationUserPurchase)


module.exports = router