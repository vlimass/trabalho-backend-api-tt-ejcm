const DataTypes = require ("sequelize")
const sequelize = require("../config/sequelize")

// Definindo User e seus atributos
const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_birth: {
        type: DataTypes.DATEONLY,
        allowNull: false
    }, 

    email: {
        type: DataTypes.STRING,
        allowNull: false
    }, 

    address: {
        type: DataTypes.STRING,
        allowNull: false
    },

    gender: {
        type: DataTypes.STRING
    },

    phone_number: {
        type: DataTypes.STRING
    }

}, {
    timestamps: false
})

// Definindo as relações de User
User.associate = (models) => {
    User.hasMany(models.Purchase, {as: "addedPurchases", foreignKey: "UserId"})
    User.hasMany(models.Comment, {as: "madeComments", foreignKey: "UserId"})
}

module.exports = User