const DataTypes = require('sequelize')
const sequelize = require('../config/sequelize')

// Definindo Comment e seus atributos
const Comment = sequelize.define('Comment', {
    title: {
        type: DataTypes.STRING, 
        allowNull: false
    }, 

    description: {
        type: DataTypes.STRING,
    },

    postedAt: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    number_of_likes: {
        type: DataTypes.NUMBER,
    }

}, {
    timestamps: false
})

// Definindo as relações de Comment
Comment.associate = (models) => {
    Comment.belongsTo(models.User)
    Comment.belongsTo(models.Purchase)
}

module.exports = Comment