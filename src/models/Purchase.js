const DataTypes = require("sequelize")
const sequelize = require("../config/sequelize")

// Definindo Purchase e seus atributos
const Purchase = sequelize.define('Purchase', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },

  price: {
    type: DataTypes.STRING,
    allowNull: false
  },

  supplier_name: {
    type: DataTypes.STRING,
    allowNull: false
  },

  isOnSale: {
    type: DataTypes.BOOLEAN
  }

},{
  timestamps: false
})

// Definindo as relações de Purchase
Purchase.associate = (models) => {
  Purchase.belongsTo(models.User)
  Purchase.hasMany(models.Comment, {as: "madeComments", foreignKey: "PurchaseId"})
}

module.exports = Purchase